package org.flamurey.gradle.plugin.flam

import org.gradle.api.Project

class FlamConfig {
    Project project

    FlamConfig(Project project) {
        this.project = project
    }

    def String getFilePath(String name) {
        String res = project.sourceSets.main.resources.find{it.name.endsWith(name)}
        if (res == null)
            res = project.sourceSets.main.java.find{it.name.endsWith(name)}
        else if (res == null)
            res = project.sourceSets.test.java.find{it.name.endsWith(name)}
        else if (res == null)
            res = project.sourceSets.test.resources.find{it.name.endsWith(name)}
        else if (res == null)
            res = "";
        return res;
    }

    def repoPath = "/home/lesha/Dropbox/repo"

    def benchmark = ''

    def needPublish = true;

    def mergeProperties(String privatePropertiesPath) {
        File propsFile = new File(privatePropertiesPath)
        if (propsFile.exists()) {
            def props = new Properties()
            propsFile.withInputStream {
                props.load it
            }
            repoPath = props.getOrDefault("flam.repoPath", "/home/lesha/Dropbox/repo")
            benchmark = props.getOrDefault("flam.benchmark", "")
        }
    }

}
