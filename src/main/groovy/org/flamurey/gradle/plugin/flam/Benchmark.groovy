package org.flamurey.gradle.plugin.flam;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

public class Benchmark {

    public static void main(String[] args) throws Exception {
        System.out.println(args[0]);
        new Runner(new OptionsBuilder()
                .include(args[0] + ".*")
                .forks(1)
                .warmupTime(TimeValue.seconds(3))
                .warmupIterations(3)
                .measurementTime(TimeValue.seconds(3))
                .measurementIterations(3)
                .build()).run();
    }
}
