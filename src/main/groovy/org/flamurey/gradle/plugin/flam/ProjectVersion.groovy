package org.flamurey.gradle.plugin.flam

class ProjectVersion {
    Integer major
    Integer minor
    Integer revision = 0
    Boolean release

    ProjectVersion(Integer major, Integer minor) {
        this.major = major
        this.minor = minor
        this.release = Boolean.FALSE
    }
    ProjectVersion(Integer major, Integer minor, Boolean release) {
        this(major, minor)
        this.release = release
    }
    ProjectVersion(Integer major, Integer minor, Integer revision, Boolean release) {
        this(major, minor, release)
        this.revision = revision
    }
    @Override
    String toString() {
        "$major.$minor.$revision${release ? '' : '-SNAPSHOT'}"
    }
}