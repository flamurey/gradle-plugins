package org.flamurey.gradle.plugin.flam
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.bundling.Jar
import org.gradle.plugins.ide.idea.IdeaPlugin
import org.gradle.testing.jacoco.plugins.JacocoPlugin

class FlamPlugin implements Plugin<Project>{

    @Override
    void apply(Project project) {
        project.plugins.apply(JavaPlugin)
        project.plugins.apply(IdeaPlugin)
        project.plugins.apply(JacocoPlugin)
        project.version = loadVersion(project);
        project.extensions.create("flam", FlamConfig, project)
        project.flam.mergeProperties(project.rootDir.absolutePath + "/private.properties")
        setDependencies(project);

        //test
        project.tasks.test.finalizedBy(project.tasks.jacocoTestReport)
        initBenchmark(project);
        project.group = "org.flamurey"
        project.afterEvaluate {
            setEnvironment(project);
            if (project.flam.needPublish) {
                project.plugins.apply(MavenPublishPlugin)
                configPublishing(project)
            }
        }
    }

    void setEnvironment(Project project) {

        project.repositories {
            maven {
                url project.flam.repoPath
            }
        }

    }

    def configPublishing(Project project) {
        Task sourcesJar = project.getTasks().create("sourcesJar", Jar) {
            classifier = 'sources'
            from project.sourceSets.main.allSource
        }

        project.publishing {
            publications {
                main(MavenPublication) {
                    from project.components.java
                    artifact project.sourcesJar
                }
            }

            repositories {
                maven {
                    url project.flam.repoPath
                }
            }
        }
    }

    def setDependencies(Project project) {
        project.repositories {
            mavenCentral()
        }

        project.dependencies {
            compile 'org.assertj:assertj-core:3.3.0'
            compile 'org.slf4j:slf4j-api:1.7.19'

            testCompile 'ch.qos.logback:logback-classic:1.1.6'
            testCompile 'org.bitbucket.flamurey:gradle-plugins:0.2.3'

            testCompile 'org.openjdk.jmh:jmh-core:1.11.3'
            testCompile 'org.openjdk.jmh:jmh-generator-annprocess:1.11.3'

            testCompile 'org.mockito:mockito-all:2.0.2-beta'
            testCompile 'org.spockframework:spock-core:1.0-groovy-2.4'
        }
    }

    def initBenchmark(Project project) {
        Task benchmark = project.task("benchmark", type: JavaExec.class, dependsOn: [project.classes, project.testClasses]) {
            group = "verification"
            classpath = project.sourceSets.test.runtimeClasspath;
            main = "org.flamurey.gradle.plugin.flam.Benchmark";
        }
        benchmark.doFirst {
            args = [project.flam.benchmark]
        }
    }

    def loadVersion(Project project) {
        try {
            def major = project.properties['major'].toInteger();
            def minor = project.properties['minor'].toInteger();
            def revision = project.properties['revision'].toInteger();
            def release = project.properties['release'].toBoolean();
            return new ProjectVersion(major,minor,revision, release);
        }
        catch (Exception e) {
            return project.version;
        }

    }
}
